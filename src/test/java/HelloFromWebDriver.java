import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class HelloFromWebDriver {
    public static void main(String[] args) {
        WebDriver driver = new ChromeDriver();
        MainPastebinPage mainPage = new MainPastebinPage(driver);
        mainPage.setCodeText("Hello from WebDriver");
        mainPage.setExpiration("10 Minutes");
        mainPage.setPasteName("helloweb");
        mainPage.submit();
        driver.quit();
    }
}
